# frozen_string_literal: true

platform = system.platform[:family]

control 'saltplusplus.package.install.python.mako.pkg.installed' do
  title 'The required pacakge should not be installed'

  package_name =
    case platform
    when 'Arch'
      'python-mako'
    else
      'python3-mako'
    end
  describe package(package_name) do
    it { should_not be_installed }
  end
end
