#!/bin/sh
# Only use this for debugging!
# set -x

########################################################################
# File managed by Salt at <{{ source }}>.
# Your changes will be overwritten.
########################################################################

# Since `GH_TOKEN` is required for creating the PR, this script doesn't do much by default
# The suggested policy is to make a copy elsewhere, override the `salt://` location in the pillar/config and
# then enter the `GH_TOKEN` below in that copy
# GH_TOKEN="{{ gh_api_key }}"
GL_TOKEN="{{ gl_api_key }}"
# Collect incoming arguments
STATE=${1}
REPO_OWNER=${2}
REPO_NAME=${3}
REPO_BRANCH=${4}
# GH_USER=${5}
BRANCH_PR=${6}
COMMIT_TITLE=${7}
COMMIT_BODY=${8}
FILE_API_RESPONSE=${9}
# Prepare initial state line variables
CHANGED='True'
COMMENT='Command `'${STATE}'` run'

# Only create the PR if it doesn't already exist
# If it already exists, the `git push` done earlier will have updated the PR already
# PR_EXISTS=$(curl -i "https://api.github.com/repos/${REPO_OWNER}/${REPO_NAME}/pulls" | grep "${GH_USER}:${BRANCH_PR}")
# if [ -n "${PR_EXISTS}" ]; then
#     CHANGED='False'
# else
#     curl -H "Authorization: bearer ${GH_TOKEN}" -d '
#         {
#             "title": "'"${COMMIT_TITLE}"'",
#             "body":  "'"${COMMIT_BODY}"'",
#             "head":  "'"${GH_USER}"':'"${BRANCH_PR}"'",
#             "base":  "'"${REPO_BRANCH}"'"
#         }
#     ' "https://api.github.com/repos/${REPO_OWNER}/${REPO_NAME}/pulls" >> "${FILE_API_RESPONSE}"
# fi

REPO_OWNER_ESCAPED="$(echo "${REPO_OWNER}" | sed 's/\//%2F/')"
PR_EXISTS=$(curl \
    --header "Authorization: Bearer ${GL_TOKEN}" \
    "https://gitlab.com/api/v4/projects/${REPO_OWNER_ESCAPED}%2F${REPO_NAME}/merge_requests?state=opened&source_branch=${BRANCH_PR}"
)
if [ "${PR_EXISTS}" != '[]' ]; then
    CHANGED='False'
else
    curl --header "Authorization: Bearer ${GL_TOKEN}" \
        --header "Content-Type: application/json" --data '
        {
            "id": "'"${REPO_OWNER_ESCAPED}%2F${REPO_NAME}"'",
            "source_branch": "'"${BRANCH_PR}"'",
            "target_branch": "'"${REPO_BRANCH}"'",
            "title": "'"${COMMIT_TITLE}"'",
            "description": "'"${COMMIT_BODY}"'",
            "remove_source_branch": "true"
        }
    ' "https://gitlab.com/api/v4/projects/${REPO_OWNER_ESCAPED}%2F${REPO_NAME}/merge_requests" >> "${FILE_API_RESPONSE}"
fi

# Write the state line
echo "changed=${CHANGED} comment='${COMMENT}'"
